<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductController@index')->name('products.index');
Route::get('/products/create', 'ProductController@create')->name('products.create')->middleware('auth', 'role:admin');
Route::post('/products', 'ProductController@store')->name('products.store')->middleware('auth', 'role:admin');
Route::post('/products/{user}/buy', 'ProductController@buy')->name('products.buy')->middleware('auth', 'role:user');
Route::get('/products/{product}', 'ProductController@show')->name('products.show');
Route::get('/products/{product}/edit', 'ProductController@edit')->name('products.edit')->middleware('can:checkOwn,product');
Route::put('/products/{product}', 'ProductController@update')->name('products.update')->middleware('can:checkOwn,product');
Route::delete('/products/{product}', 'ProductController@destroy')->name('products.destroy')->middleware('can:checkOwn,product');

//Route::resource('products', 'ProductController');

Route::get('/login', 'LoginController@index')->name('login.index');
Route::post('/login', 'LoginController@login')->name('login');
Route::post('/logout', 'LoginController@logout')->name('logout');
