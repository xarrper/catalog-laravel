<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'id' => 1,
            'username' => 'Mozart',
            'password' => bcrypt('123456'),
            'role_id' => 1
        ], [
            'id' => 2,
            'username' => 'Chopin',
            'password' => bcrypt('123456'),
            'role_id' => 2
        ]]);
    }
}
