<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([[
            'id' => 1,
            'title' => 'Телевизор Samsung UE43N5000AU',
            'description' => 'ЖК-телевизор, 1080p Full HD, диагональ 42.5" (108 см), HDMI x2, USB, DVB-T2, тип подсветки: Edge LED. картинка в картинке',
            'cost' => 41586,
            'user_id' => 1,
            'category_id' => 1,
        ], [
            'id' => 2,
            'title' => 'Телевизор LG 20MT48VF',
            'description' => 'ЖК-телевизор, 720p HD, диагональ 20" (51 см), TFT TN, HDMI, USB, DVB-T2, тип подсветки: Edge LED',
            'cost' => 22530,
            'user_id' => 1,
            'category_id' => 1,
        ], [
            'id' => 3,
            'title' => 'Телевизор Thomson T32RTL5140',
            'description' => 'ЖК-телевизор, 720p HD, диагональ 31.5" (80 см), Smart TV, Wi-Fi, HDMI x2, USB x2, DVB-T2, тип подсветки: Direct LED',
            'cost' => 35760,
            'user_id' => 1,
            'category_id' => 1,
        ], [
            'id' => 4,
            'title' => 'Смартфон Xiaomi Redmi',
            'description' => 'смартфон на платформе Android, поддержка двух SIM-карт, экран 6.25", разрешение 2280x1080',
            'cost' => 6400,
            'user_id' => 1,
            'category_id' => 2,
        ], [
            'id' => 5,
            'title' => 'Смартфон HUAWEI P Smart (2019) 3/32GB',
            'description' => 'смартфон с Android 9.0, поддержка двух SIM-карт, экран 6.21", разрешение 2340x1080',
            'cost' => 17800,
            'user_id' => 1,
            'category_id' => 2,
        ], [
            'id' => 6,
            'title' => 'Смартфон Samsung Galaxy A30 SM-A305F 32GB',
            'description' => 'смартфон с Android 9.0, поддержка двух SIM-карт, экран 6.4", разрешение 2340x1080',
            'cost' => 21450,
            'user_id' => 1,
            'category_id' => 2,
        ], [
            'id' => 7,
            'title' => 'Смартфон ASUS Zenfone Lite',
            'description' => 'смартфон с Android 8.0, поддержка двух SIM-карт, экран 5.5", двойная камера 13 МП/8 МП, автофокус',
            'cost' => 16700,
            'user_id' => 1,
            'category_id' => 2,
        ]]);
    }
}
