"use strict";

document.addEventListener('DOMContentLoaded', () => {

    function clearFilter() {
        let nameFilter = ['#productSearch', '#productCategory', '#productCategory', '#productCost input'];
        nameFilter.forEach((item) => {
            $(item).val('');
        });
    }

    function removeParamFromUrl() {
        window.history.pushState({}, document.title, "/");
    }

    function resetForm() {
        removeParamFromUrl();
        clearFilter();
    }

    function subscribeToChannel() {
        window.Echo.private('buy')
            .listen('PurchaseEvent', (data) => {
                console.log(data.message);
            });
    }

    subscribeToChannel();

});

