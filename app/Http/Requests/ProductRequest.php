<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\src\Helpers\AppHelper;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function sanitize()
    {
        $existFields = ['title', 'description', 'cost', 'category_id'];

        $input = AppHelper::filterArrayByExistKey($this->all(), $existFields);

        $this->replace($input);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $this->sanitize();

        return [
            'title' => 'required',
            'description' => 'required',
            'category_id' => 'required|exists:categories,id',
            'cost' => 'required|numeric',
        ];
    }
}
