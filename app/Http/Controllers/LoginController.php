<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function index(): \Illuminate\View\View
    {
        if (Auth::check()) {
            return back();
        }
        return view('login');
    }

    public function login(Request $request): \Illuminate\Http\RedirectResponse
    {
        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->route('products.index');
        } else {
            return back()->with(['message' => 'Неверный логин или пароль']);
        }

    }

    public function logout(): \Illuminate\Http\RedirectResponse
    {
        Auth::logout();
        return back();
    }

}
