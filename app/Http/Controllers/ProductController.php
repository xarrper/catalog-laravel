<?php

namespace App\Http\Controllers;

use App\Events\PurchaseEvent;
use App\src\Models\Category;
use App\Http\Requests\ProductRequest;
use App\src\Filters\ProductFilter;
use App\src\Models\Product;
use App\src\Models\User;
use App\src\Repositories\ProductRepository;
use App\src\Repositories\Repository;

class ProductController extends Controller
{

    private $categoryRepository;
    private $productRepository;

    function __construct(Category $categories, ProductRepository $productRepository)
    {
        $this->categoryRepository = new Repository($categories);
        $this->productRepository = $productRepository;
    }

    public function index(ProductFilter $filters): \Illuminate\View\View
    {
        $categories = $this->categoryRepository->all();
        $products = $this->productRepository->filter($filters);

        return view('products.list', [
            'products' => $products,
            'categories' => $categories
        ]);
    }

    public function show(Product $product): \Illuminate\View\View
    {
        return view('products.show', ['product' => $product]);
    }

    public function create(): \Illuminate\View\View
    {
        return view('products.create', [
            'categories' => $this->categoryRepository->all()
        ]);
    }

    public function store(ProductRequest $request): \Illuminate\Http\RedirectResponse
    {
        $this->productRepository->create($request->all());
        return back()->with(['message' => 'Данные успешно добавлены']);
    }

    public function edit(Product $product): \Illuminate\View\View
    {
        return view('products.edit', [
            'categories' => $this->categoryRepository->all(),
            'product' => $product
        ]);
    }

    public function update(ProductRequest $request, Product $product): \Illuminate\Http\RedirectResponse
    {
        $this->productRepository->update($request->all(), $product->id);
        return back()->with(['message' => 'Данные успешно изменены']);
    }

    public function destroy(Product $product): \Illuminate\Http\RedirectResponse
    {
        $this->productRepository->delete($product->id);
        return back();
    }

    public function buy(User $user): \Illuminate\Http\RedirectResponse
    {
        PurchaseEvent::dispatch($user->username);
        return back();
    }

}
