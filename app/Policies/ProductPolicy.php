<?php

namespace App\Policies;

use App\src\Models\User;
use App\src\Models\Product;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    public function checkOwn(User $user, Product $product): bool
    {
        return $user->id === $product->user_id;
    }

}
