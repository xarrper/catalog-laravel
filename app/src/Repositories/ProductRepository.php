<?php

namespace App\src\Repositories;

use App\src\Models\Product;
use Illuminate\Support\Facades\Auth;
use App\src\Filters\ProductFilter;
use Illuminate\Database\Eloquent\Collection;

class ProductRepository extends Repository
{
    public function __construct(Product $model)
    {
        parent::__construct($model);
    }

    public function create(array $data): bool
    {
        $this->model->user_id = Auth::user()->id;
        return parent::create($data);
    }

    public function filter(ProductFilter $filters): Collection
    {
        return $this->model->filter($filters)->get();
    }

}