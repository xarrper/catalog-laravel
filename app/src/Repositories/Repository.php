<?php

namespace App\src\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;

class Repository implements RepositoryInterface
{

    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function all(): Collection
    {
        return $this->model->all();
    }

    public function create(array $data): bool
    {
        $this->model->fill($data);
        return $this->model->save();
    }

    public function update(array $data, int $id): bool
    {
        return $this->model->find($id)
            ->update($data);
    }

    public function delete(int $id): int
    {
        return $this->model->destroy($id);
    }

    public function show(int $id): Builder
    {
        return $this->model->findOrFail($id);
    }


}
