@if (session()->has('message'))
    <div class="alert alert-{{$type}}" role="alert">
        {{ session()->get('message') }}
    </div>
@endif