@if($errors->has($value))
    <div class="invalid-feedback d-block">
        @foreach($errors->get($value) as $message)
            {{ $message }}
        @endforeach
    </div>
@endif