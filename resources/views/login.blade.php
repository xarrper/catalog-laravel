@extends('layouts.app')

@section('title', 'Авторизация')

@section('content')
    <div class="text-center">
        <form class="form-signin" action="{{route('login')}}" method="post">
            @csrf
            <h1 class="h3 mb-3 font-weight-normal">Войти</h1>
            <label for="inputUsername" class="sr-only">Логин</label>
            <input type="text" id="inputUsername" class="form-control" placeholder="Логин" name="username" required autofocus>
            <label for="inputPassword" class="sr-only">Пароль</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Пароль" name="password" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
        </form>
    </div>
    @include('shared.alert', ['type' => 'danger'])
@endsection
