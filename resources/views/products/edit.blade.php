@extends('layouts.app')

@section('title', 'Редактирование')

@section('content')

    @extends('layouts.menu')

    <main role="main" class="container">
        <form method="post" action="{{route('products.update', $product->id)}}">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="titleProduct">Название товара</label>
                <input type="text" class="form-control @include('shared.class-errors', ['value' => 'title'])" id="titleProduct"
                       value="{{$product->title}}" placeholder="Введите название" name="title" required autofocus>

                @include('shared.errors', ['value' => 'title'])

            </div>
            <div class="form-group">
                <label for="descriptionProduct">Описание товара</label>
                <textarea class="form-control @include('shared.class-errors', ['value' => 'description'])" id="descriptionProduct" rows="3" name="description" required>{{$product->description}}</textarea>

                @include('shared.errors', ['value' => 'description'])

            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="categoryProduct">Категория товара</label>
                        <select class="form-control @include('shared.class-errors', ['value' => 'category_id'])" id="categoryProduct" name="category_id">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}" {{ $product->category_id == $category->id ? 'selected' : '' }}>{{$category->title}}</option>
                            @endforeach
                        </select>

                        @include('shared.errors', ['value' => 'category_id'])

                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="costProduct">Стоимость товара</label>
                        <input type="text" class="form-control @include('shared.class-errors', ['value' => 'cost'])" value="{{$product->cost}}" id="costProduct" placeholder="Введите стоимость" name="cost" required>

                        @include('shared.errors', ['value' => 'cost'])

                    </div>
                </div>
            </div>
            <div class="float-right">
                <button type="submit" class="btn btn-success">Обновить</button>
            </div>
            <div class="clearfix"></div>
        </form>
        @include('shared.alert', ['type' => 'success'])
    </main>

@endsection
