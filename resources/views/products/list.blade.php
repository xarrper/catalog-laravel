@extends('layouts.app')

@section('title', 'Список продуктов')

@section('content')

    @extends('layouts.menu')

    <main role="main" class="container">

        <section class="jumbotron text-center">
            <div class="container">
                <h1 class="jumbotron-heading">ООО "Техника"</h1>
                <p class="lead text-muted">Общество с ограниченной ответственностью "Техника"</p>
                <p>
                    <small class="text-muted">414056, г. Астрахань,ул. Куйбышева, 63/72/4, литер А, пом.031</small>
                    <small class="text-muted">www.tehrantour.ru</small>
                </p>
            </div>
        </section>

        <form action="{{route('products.index')}}">
            <div class="py-5 bg-light">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="productSearch">Поиск</label>
                                <input class="form-control" id="productSearch" placeholder="Поиск..." name="search" value="{{ request()->search }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="productCategory">Категория</label>
                                <select class="form-control" id="productCategory" name="category">
                                    <option {{ !request()->category ? 'selected' : '' }} value="">Выберите...</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" {{ request()->category == $category->id ? 'selected' : '' }}>{{$category->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="productCost">Стоимость</label>
                                <div class="input-group" id="productCost">
                                    <input type="text" class="form-control" placeholder="От" name="costFrom" value="{{ request()->costFrom }}">
                                    <input type="text" class="form-control" placeholder="До" name="costTo" value="{{ request()->costTo }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="float-right">
                        <button type="submit" class="btn btn-secondary" onclick="resetForm()">Сброс</button>
                        <button type="submit" class="btn btn-info">Применить</button>
                    </div>
                </div>
            </div>
        </form>

        <div class="album py-5 bg-light">
            <div class="container">
                <!-- TODO: логика -->
                @if(Auth::check() && Auth::user()->role->title == 'admin')
                    <div class="float-right cst-margin-20">
                        <a href="{{route('products.create')}}"><button type="button" class="btn btn-success">Создать</button></a>
                    </div>
                    <div class="clearfix"></div>
                @endif

                <div class="row">
                    @foreach($products as $product)
                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                <div class="card-body">
                                    <!-- TODO: логика -->
                                    @can('checkOwn', $product)
                                        <div class="float-right">
                                            <a class="cst-link-icon" href="{{ route('products.edit', $product) }}"><i class="fa fa-pencil"></i></a>
                                            <form class="d-inline-block" method="post" action="{{route('products.destroy', $product)}}">
                                                @csrf
                                                @method('DELETE')
                                                <button class="cst-btn-icon"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                    @endcan

                                    <p class="card-text">{{$product->title}}</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div>
                                            <a class="cst-link-none" href="{{route('products.show', $product->id)}}">
                                                <button type="button" class="btn btn-sm btn-outline-secondary">Подробнее
                                                </button>
                                            </a>
                                            <!-- TODO: логика -->
                                            @if(Auth::check() && Auth::user()->role->title == 'user')
                                                <form class="d-inline-block" method="post" action="{{route('products.buy', Auth::user())}}">
                                                    @csrf
                                                    <button class="btn btn-sm btn-success">Купить</button>
                                                </form>
                                            @endif
                                        </div>
                                        <small class="text-muted">{{$product->cost}}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

    </main>

    @push('scripts')
        <script src="/js/script.js"></script>
    @endpush
@endsection
