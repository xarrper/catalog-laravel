@inject('APP', 'App\src\Helpers\AppHelper')

@extends('layouts.app')

@section('title', $product->title)

@section('content')

    @extends('layouts.menu')

    <main role="main" class="container">

        <section class="jumbotron text-center">
            <div class="container">
                <h1 class="jumbotron-heading">{{$product->title}}</h1>
            </div>
        </section>

        <div class="album py-5 bg-light">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card mb-12 box-shadow">
                            <div class="card-body">
                                <p class="card-text">{{$product->description}}</p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <small class="text-muted">{{$APP::productCost($product->cost)}}</small>
                                    <small class="text-muted">{{$product->category->title}}</small>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </main>
@endsection
