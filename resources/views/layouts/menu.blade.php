<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{route('products.index')}}">Главная</a>
            </li>
        </ul>
    </div>
    <ul class="navbar-nav mr-auto">
        <!-- TODO: логика -->
        @if(Auth::check())
            <li class="nav-item">
                <p class="nav-link cst-margin-0">{{Auth::user()->username}} : {{Auth::user()->role->title}}</p>
            </li>
            <form method="post" action="{{route('logout')}}" class="cst-margin-0">
                @csrf
                <li class="nav-item active">
                    <button class="nav-link cst-btn-link">Выход</button>
                </li>
            </form>
        @else
            <li class="nav-item active">
                <a class="nav-link" href="{{route('login.index')}}">Вход</a>
            </li>
        @endif

    </ul>
</nav>